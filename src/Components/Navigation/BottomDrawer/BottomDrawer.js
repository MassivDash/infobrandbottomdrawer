import React from 'react';
import './BottomDrawer.css';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Auxy from '../../../hoc/auxy';


const BottomDrawer = ( props ) => {
  let attachedClasses = [ "BottomDrawer", "Close" ];
  if (props.open) {
    attachedClasses = ["BottomDrawer", "Open"];
    document.body.style.overflow = "hidden";
     
  }
  else {
     document.body.style.overflow = "auto";
    
  }
return (
<Auxy>
  <Backdrop show={props.open} clicked={props.closed}/>
  <div className={attachedClasses.join(' ')}><p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam.
      </p>
      <button onClick={() => console.log('clicked!')}>Click me</button><br/>
      <label>check me
        <input type="checkbox" onClick={() => console.log('checked!')} />
      </label>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam.
      </p>
      <button onClick={() => console.log('clicked!')}>Click me</button><br/>
      <label>check me
        <input type="checkbox" onClick={() => console.log('checked!')} />
      </label>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam.
      </p>
      <button onClick={() => console.log('clicked!')}>Click me</button><br/>
      <label>check me
        <input type="checkbox" onClick={() => console.log('checked!')} />
      </label>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam.
      </p>
      <button onClick={() => console.log('clicked!')}>Click me</button><br/>
      <label>check me
        <input type="checkbox" onClick={() => console.log('checked!')} />
      </label>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam.
      </p>
      <button onClick={() => console.log('clicked!')}>Click me</button><br/>
      <label>check me
        <input type="checkbox" onClick={() => console.log('checked!')} />
      </label>


  </div>
</Auxy>
);

};

export default BottomDrawer
