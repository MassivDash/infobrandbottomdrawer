import React, { Component } from 'react';
import Auxy from '../../hoc/auxy';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import BottomDrawer from '../Navigation/BottomDrawer/BottomDrawer';


class Layout extends  Component {
  state= {
    showBottomDrawer: false
  }

  bottomDrawerClosedHandler = () => {
    this.setState({showBottomDrawer: false});

  }

  bottomDrawerToogleHandler = () => {
    this.setState(( prevState ) => {
      return { showBottomDrawer: !prevState.showBottomDrawer};
     
      
  } );
}



render () {



  return (
      <Auxy>
        <Toolbar
        drawerToogleClicked={this.bottomDrawerToogleHandler}/>
        <BottomDrawer
          open={this.state.showBottomDrawer}
          closed={this.bottomDrawerClosedHandler}/>
        <main>
          {this.props.children}
        </main>
      </Auxy>
    )
  };
}

export default Layout;
